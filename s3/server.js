const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.delete('/delete',(req,res)=>{ 
    let ok=0;
    var productsF = products.filter(p => req.body.productName != p.productName);
    if(productsF.length != products.length)ok=1;
    if(ok==1){
        products=productsF;
        var len=products.length;
        res.status(200).send(`Produsele cu numele ${req.body.productName} au fost sterse, dimensiunea actuala a vectorului de produse: ${len.toString()}`);
    }
    else{
        res.status(404).send(`Produsul nu exista in lista de produse!`)
    }
});

app.put('/update/:id',(req,res,next)=>{
    const idp=req.params.id;
    let updated=false;
    for(let i=0;i<products.length;i++){
        if(products[i].id==idp){
            updated=true;
            products[i].productName=req.body.productName;
            products[i].price=req.body.price;
        }
    }
    if(updated==true){
        res.status(200).send(`Produsul ${idp} a fost modificat!`);
    }
    else{
        res.status(404).send(`Produsul nu a fost gasit!`);
    }
});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});