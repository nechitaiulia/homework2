import React from 'react';
class AddProduct extends React.Component {
  constructor(props){
      super(props)
      this.state={
          productName:'',
          price:0
      }
  }
  
      handleChangePrice = (e) => {
        this.setState({
           price: e.target.value
        });
        
      }
    
       handleChangeProductName = (e) => {
        this.setState({
            productName: e.target.value
        });
        
    }
  
  render() {
    return (
      <div>
        <form >
            <label htmlFor="productName">Nume produs</label>
            <input type="text" name="productName" id="productName" onChange={this
                .handleChangeProductName} />
            <label htmlFor="price">Pret</label>
            <input type="number" name="price" id="price" onChange={this
                .handleChangePrice}/>
            <input type="button" value="add" onClick={() => this.props.onAdd({
      			productName : this.state.productName,
      			price : this.state.price
      		})} />
        </form>
      </div>
    );
  }
}

export default AddProduct;
