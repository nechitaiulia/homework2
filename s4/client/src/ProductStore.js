import axios from 'axios'
import {EventEmitter} from 'fbemitter'

const SERVER='http://examen-nechiulia.c9users.io:8080'
class ProductStore{
    constructor(){
        this.content=[]
        this.emitter = new EventEmitter()
    }
    
    async getAll(){
        try{
            let response=await axios(`${SERVER}/get-all`)
            this.content=response.data
            this.emitter.emit('GET_ALL_SUCCESS')
        }
        catch(ex){
            console.log(ex)
            this.emitter.emit('GET_ALL_ERROR')
        }
    }
    
    async addOne(product){
        try{
            
            let response=await axios.post(`${SERVER}/add`,product)
            this.getAll()
            this.emitter.emit('ADD_SUCCES')
            console.log("da")
        }
        catch(ex){
            console.log(ex)
            this.emitter.emit('ADD_ERROR')
        }
    }
    
}

export default ProductStore;