import React from 'react';
import ProductStore from './ProductStore'
import AddProduct from './AddProduct'

class ProductList extends React.Component {
    constructor(){
        super()
        this.state={
            products:[]
        }
        this.store=new ProductStore()
        this.add=(product)=>{
            this.store.addOne(product)
        }
    }
    
    componentDidMount(){
        this.store.getAll()
        this.store.emitter.addListener('GET_ALL_SUCCESS',()=>{
            this.setState({
                products:this.store.content
            })
        })
    }
    
    render(){
        return (
            <div>
                <div>
                    {
                        this.state.products.map((e,i)=><div key={i}>
                        {e.productName}{e.price}
                        </div>
                        )
                    }
                </div>
                <AddProduct onAdd={this.add} />
            </div>
            );
    }    
}
export default ProductList;